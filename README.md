# lockfile-tampering-examples

Examples of tampered lockfiles for different package managers.

See `README.MD` in the individual folders for explanation
and instructions on how to run the examples.
