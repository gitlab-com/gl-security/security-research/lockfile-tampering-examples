#!/bin/bash

yarn use version berry
yarn -v

# installs from tampered lockfile
yarn install

# dumporify was installed instead of dompurify
ls .yarn/cache

# install script of dumporify was executed
cat /tmp/lol
