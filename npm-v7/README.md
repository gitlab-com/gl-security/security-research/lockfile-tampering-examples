## What is this

Lockfile tampering example for npm v7.

## How to run the example

```sh
docker build -t npm-v7-example .
docker run npm-v7-example
```