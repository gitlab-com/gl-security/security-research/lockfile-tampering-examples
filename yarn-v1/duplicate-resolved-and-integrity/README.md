## What is this

Another example of tampering `yarn.lock`. This example demonstrates
that yarn is forgiving when parsing lockfiles with duplicate entries 
for `resolved` and `integrity`. Yarn will only use the last occurence 
of the entry and ignore the others.

## How is it done

```sh
diff yarn-original.lock yarn.lock
6124a6125
>   resolved "https://registry.yarnpkg.com/dumporify/-/dumporify-1.0.0.tgz#7c8fce8f42f33f121f73a399577e84cede72ac65"
6125a6127
>   integrity sha512-/7M3iY3fI/mA4xLFYp+2fLP37nRg9LEPD60v3YOfHt8+yFp5sQMZdEYyhleyIQVBDdX42U4WKdTKDnwdBor87w==
```

The sources of `dompurify` are replaced with the source's of an alternative package `dumporify`.
When `resolved` and `integrity` appear multiple times for the same lockfile entry, the last occurance
is used.

## How to run the example

```sh
docker build -t yarn-v1-lockfile-dupe-example .
docker run yarn-v1-lockfile-dupe-example
```